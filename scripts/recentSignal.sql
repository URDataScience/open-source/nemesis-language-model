SELECT
  *
FROM (
	  SELECT
	    sick.user_id AS user_id,
	    sick.sick_score AS sick_score,
	    sick.tweet_text AS sick_tweet_text,
	    snap.tweet_text AS snap_tweet_text,
	    sick.timestamp AS sick_timestamp,
	    snap.timestamp AS snap_timestamp,
	    snap.closest_venue.gp.name as venue_name,
	    snap.closest_venue.gp.address as venue_address
	  FROM
	    [ur-twitter-001:nEmesis.sick] AS sick
	  INNER JOIN
	    [ur-twitter-001:nEmesis.snap] AS snap
	  ON
	    sick.user_id = snap.user_id
) AS signal
WHERE
  sick_timestamp > snap_timestamp
  AND DATEDIFF(sick_timestamp,snap_timestamp) < 6
  AND DATEDIFF(CURRENT_DATE(),sick_timestamp) <= 7
  AND sick_score > 0.75
ORDER BY
    sick_score DESC,
    sick_timestamp DESC
