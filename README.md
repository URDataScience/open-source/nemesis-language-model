# nemesis-labeler

To use this code:
- Create a directory called labeler
- Clone this repo into that directory
- The processes to run are in the scripts directory (see scripts/labeljobv1.sh)
- These (currently) have expected paths defined within them which outline where the input files are expected to be, and where the output files will be placed.  <In future release we should parameterize everything so we can have a single labeler code base that can operate off multiple SVM's, and input/output paths>

This code uses an SVM libary created by Thorsten Joachims.  See the folder svm_perf.