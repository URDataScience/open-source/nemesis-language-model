#!/bin/sh
trackdir=$1
labeldir=$2
todayfile=$(date +%d-%m-%Y).json
#wordfile="/home/vax7/p7/twitter2/lasvegasConcept2014/svm-codes/words-food.txt"
wordfile="/p/twitter3/lasvegas/labeler/scripts/words-food.txt"
skipfile=0
processedfile=0
#gsutildir="/u/jteitel2/gsutil"
googleapi="/u/eportman/google-cloud-sdk/bin"

date
echo "Tweet root directory to search: $trackdir" #this needs to be the directory above which all raw tweet files pre snap are located
echo "Target label file location: $labeldir" # this needs to be the directory above which all labeled files are located


for file in `find $trackdir -name \*_tracked.json -mtime -100`; do
	echo "Process track file: $file"
	track_subdir=$(dirname $file | sed -e's/^.*\///g')
	echo $track_subdir
	basefilename=$(basename $file "_tracked.json") # grab the filename from the path ###need to change to _geo.json
	echo "The filename was: $basefilename"
	date
	
	



	if [ $basefilename == $todayfile ]; then ##skip todays file because it is still being written to
		skipfile=$(($skipfile + 1))
		continue
	fi

	fulloutputdir=$labeldir"/"$track_subdir
	if [ ! -d $fulloutputdir ]; then	
		mkdir -p $fulloutputdir
		chmod 774 $fulloutputdir
	fi
	fixedfile=$fulloutputdir"/"$basefilename"_fixed.json"
	echo $fixedfile	
	cat $file | sed -e's/^$/removethisisempty/' | grep -v removethisisempty > $fixedfile
	
	featurefile=$fulloutputdir"/"$basefilename"_features.dat"
	predictionfile=$fulloutputdir"/"$basefilename"_prediction.dat"
	beinglabeledfile=$fulloutputdir"/"$basefilename"_beinglabeled.json"
	labeledfile=$fulloutputdir"/"$basefilename"_labeled.json"
	#gsutillabeledfile="gs://th-001/labeled/"$track_subdir"/"$basefilename"_labeled.json"
	#httpurlfile="http://persuasive-ace-726.appspot.com/aiservice/loadtweets/labeled/"$track_subdir"/"$basefilename"_labeled.json"
	if [ -s $labeledfile ]; then
		skipfile=$(($skipfile + 1))
		continue
	else
		echo "Output to filename: $featurefile"
		date
		python -OO /p/twitter3/lasvegas/labeler/scripts/createFeatureDataSVM.py $fixedfile $wordfile $featurefile
		date
		if [ $? == 0 ]; then
			echo "Classify Tweets"	
			date
			#/p/twitter2/lasvegasConcept2014/svm_perf/svm_perf_classify -v 0 $featurefile /p/twitter2/lasvegasConcept2014/svm_perf/svm_final $predictionfile
			/p/twitter3/lasvegas/labeler/scripts/svm_perf/svm_perf_classify -v 0 $featurefile /home/vax8/u1/eportman/rocdata/RocData/nEmesis/svm_final $predictionfile
			date
			echo "I am now here"
		else
			echo "Feature Creation failed"
			exit 1
		fi
		if [ $? == 0 ]; then
			touch $beinglabeledfile
			echo "Apply Label"
			date
			python /p/twitter3/lasvegas/labeler/scripts/applyLabel.py $fixedfile $predictionfile .5 $beinglabeledfile false
			date
		else
			echo "Classification failed"
			exit 1
		fi
		if [ $? == 0 ]; then
			mv $beinglabeledfile $labeledfile
			echo "rename file"
		else
			echo "Apply Label failed"
			exit 1
		fi
			
		if [ $? == 0 ]; then
			chmod 774 $labeledfile
			chmod 774 $fixedfile
			rm $predictionfile
			rm $featurefile
			rm $fixedfile

			cat $labeledfile | /u/eportman/jq/jq -c '.|{metro:"lav",tweet_id:.id_str,user_id:.user.id_str,tweet_text:.text,sick_score:.sickscore,timestamp:(.timestamp_ms|tonumber| . / 1000)}' > $labeledfile".bqload"
			bqpartitiondate=$(basename $labeledfile | sed -e's/\(..\)-\(..\)-\(....\)_.*/\3\2\1/')	
			$googleapi/bq load --replace --source_format NEWLINE_DELIMITED_JSON nEmesis.sick'$'$bqpartitiondate $labeledfile".bqload" /p/twitter3/lasvegas/labeler/scripts/bq_sick_tweets_schema.json

		else
			echo "Curl failed"
			exit 1	

		fi
		processedfile=$(($processedfile + 1))
	fi
	
done

echo "Skipped $skipfile files"
echo "Processed $processedfile files"
runDate=$(date +%Y%m%d)
signalFile="/tmp/recentSignal"$runDate".csv"
gsSignalFile="gs://ur-nemesis/lavsignal/recentSignal"$runDate".csv"
signalFileURL="https://storage.cloud.google.com/ur-nemesis/lavsignal/recentSignal"$runDate".csv"
cat /p/twitter3/lasvegas/labeler/scripts/recentSignal.sql | $googleapi/bq query --format csv > $signalFile
$googleapi/gsutil cp $signalFile $gsSignalFile

echo "Attached csv for the daily signal for $runDate." | mailx -s "$runDate Twitter Signal" -a $signalFile eportman@alum.mit.edu,diprete@SNHDMAIL.ORG,Bergtholdt@snhdmail.org
