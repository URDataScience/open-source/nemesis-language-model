"""
Transform each tweet in the input JSON file into the SVM feature space. 

Example:
    python -OO createTestingDataSVM.py data/2012/m_SVM_labeled_all.json words-food.txt /data/feature/output.json

"""
import os
import sys
from extract_features import *

tweetsFile = sys.argv[1]
wordsFile = sys.argv[2]
featureFile = sys.argv[3]
baseDir = os.path.dirname(tweetsFile)
baseName = os.path.splitext(os.path.basename(tweetsFile))[0]
WORDStoID = readUniverseOfWordsFoodSVM(wordsFile)
WORDS = frozenset(WORDStoID.keys())

f_in = open(tweetsFile)
f_features = open(featureFile, 'w+')
counter = 1
for line in f_in:
	try: 
		tweet = json.loads(line)
	except:
		print counter
		print "Bad: %s" % line
		tweet={}
	f_features.write('%s\n' % getFeatureString(tweet, WORDS, WORDStoID))
	counter = counter + 1
f_features.close()
f_in.close()

