#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import HTMLParser
import numpy
import re
import string
import sys
import time
import ujson as json

p = re.compile(r'^#*[a-z]+[\'-/]*[a-z]*$', re.UNICODE)
pLink = re.compile(r'https*:\S+\.\w+', re.IGNORECASE)
pMention = re.compile(r'@[A-Za-z0-9_]+\b')
pNewLine = re.compile(r'[\r\n]+')
pRetweet = re.compile(r'\brt\b', re.IGNORECASE)
punctuation = { 0x2018:0x27, 0x2019:0x27, 0x201C:0x22, 0x201D:0x22 }
h = HTMLParser.HTMLParser()
# The word token feature IDs will start from numAdditionalFeatures+1
numAdditionalFeatures = 2 # RTfeature, fluFeature

def normalizeHealth(h):
    """ Transform a potentially unbounded health score into a [0...1] interval """
    minHealth = -4.0
    maxHealth = 4.0
    # Make sure health is within bounds
    if h < minHealth:
        h = minHealth
    elif h > maxHealth:
        h = maxHealth
    return (h - minHealth)/(maxHealth-minHealth)

def isRetweet(text):
    return int(bool(pRetweet.match(text)))

def readUniverseOfWordsFoodSVM(fileName):
    f_words = open(fileName, 'r')
    WORDStoID = {}
    for (idx, w) in enumerate(f_words):
        #if idx%100000==0 and idx>0:
            #print '%d tokens read' % idx
        WORDStoID[w.strip()] = idx + 1 + numAdditionalFeatures
    f_words.close()
    print 'Number of distinct tokens: %d' % len(WORDStoID.keys())
    return WORDStoID

def readUniverseOfWords(fileName):
    f_words = open(fileName, 'r')
    WORDStoID = {}
    for (idx, w) in enumerate(f_words):
        #if idx%100000==0 and idx>0:
            #print '%d tokens read' % idx
        splitted = w.strip().split(' ')
        if len(splitted) == 1:
            WORDStoID[splitted[0]] = idx + 1
        else:
            WORDStoID[tuple(splitted)] = idx + 1
    f_words.close()
    print 'Number of distinct tokens: %d' % len(WORDStoID.keys())
    return WORDStoID

def normalizeOrRemoveWord(w, p):
    if pLink.match(w):
        return '[::LINK::]'
    if pMention.match(w):
        return '[::@::]'
    if type(w) is unicode:
        w = w.translate(punctuation).encode('ascii', 'ignore') # find ASCII equivalents for unicode quotes
    if len(w) < 1:
        return None
    if w[0] == '#':
        w = w.strip('.,*;-:"\'`?!)(').lower()
    else:
        w = w.strip(string.punctuation).lower()
    #if w[0] == '@' or w.find('www.') != -1 or  w.find('http://') != -1 or w.find('rt',0,3) != -1:
    #if w == 'rt':
    #    continue
    if not(p.match(w)):# and not(w[0] == '#'):
        #print w
        return None
    return w

def normalizeOrRemoveWordOld(w, p):
    w = w.strip('.,;-:"\'?!)(').lower()
    if not(p.match(w)):# and not(w[0] == '#'):
        return None
    return w

def filterWords(text, p=p):
    """
    Keep only words that pass normalizeOrRemoveWord(), return them as a list
    """
    words = text.split()
    out = []
    for w in words:
        w = normalizeOrRemoveWord(w, p)
        if w != None:
            out.append(w)
    return out

def filterWordsOld(text, p=p):
    """
    Keep only words that pass normalizeOrRemoveWord(), return them as a list
    """
    words = text.split()
    out = []
    for w in words:
        w = normalizeOrRemoveWordOld(w, p)
        if w != None:
            out.append(w)
    return out

def getFeatureString(tweet, WORDS, WORDStoID):
    try:
        text = re.sub(pNewLine, ' ', h.unescape(tweet['text'])) # Remove newlines
    except:
        text=""
    RTfeature = isRetweet(text)
    fluFeature = 0.0
    #if fluFeature > 0.8:
    #    print text
    words = filterWords(text)
    # single words
    TOKENS = set(words)
    # word pairs
    for i in range(0,len(words)-1):
        TOKENS.add(' '.join((words[i], words[i+1])))
    # word triples
    for i in range(0,len(words)-2):
        TOKENS.add(' '.join((words[i], words[i+1], words[i+2])))

    intersect = TOKENS.intersection(WORDS)
    features = []
    for w in intersect:
        features.append(WORDStoID[w])
    try:
        featureString = '%+d ' % tweet['food_label']
    except KeyError:
        featureString = '+1 '
    featureString += '1:%d ' % RTfeature
    featureString += '2:%.4f ' % fluFeature
    for f in sorted(features):
        featureString += '%d:1 ' % f
    featureString += ' # %s' % text.encode('utf-8')
    return featureString

##############################################################

def loadSVM(fileName):
    f_model = open(fileName, 'r')
    lines = f_model.readlines()
    f_model.close()
    D = int(lines[0])
    b = float(lines[1])
    alphaY = float(lines[2])
    featureWeights = lines[3].strip().split(' ')
    model = numpy.zeros(D)
    assert(len(model) > 0)
    for f in featureWeights:
        (num, w) = f.split(':')
        model[int(num)] = float(w)
    return model

def classifyTweetPython(text, WORDStoID, model):
    # Transform text into SVM feature space
    words = filterWordsOld(text, p)
    # single words
    TOKENS = set(words)
    # word pairs
    for i in xrange(0,len(words)-1):
        TOKENS.add( (words[i], words[i+1]) )
    # word tripples
    for i in xrange(0,len(words)-2):
        TOKENS.add( (words[i], words[i+1], words[i+2]) )

    score = 0
    for word in TOKENS:
        try:
            word_id = WORDStoID[word]
        except KeyError:
            continue
        score += model[word_id]
        #print '%.4f: %s = %d' % (score, word, word_id)
    return score
