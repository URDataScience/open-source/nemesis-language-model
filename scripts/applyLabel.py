import sys
import ujson as json

tweetsFile = sys.argv[1]
predictionFile = sys.argv[2]
threshold = sys.argv[3]
outputFile = sys.argv[4]
writealltweets = sys.argv[5]

f_tweet = open(tweetsFile)
f_pred = open(predictionFile)
f_output = open(outputFile, 'w+')
counter = 1
sickcounter = 0
for line in f_tweet:
	try: 
		tweet = json.loads(line)
		predline = f_pred.readline()
		tweet['sickscore']=float(predline)
		if tweet['sickscore']>=float(threshold):
			sickcounter = sickcounter + 1
			tweet['sicklabel']=1
			f_output.write(json.dumps(tweet)+'\n')
		else:
			tweet['sicklabel']=-1
			if writealltweets == "true":
				f_output.write(json.dumps(tweet)+'\n')
	except:
		print counter
		print "Bad: %s" % line
		sys.exit(1)
	counter = counter + 1
print "Sick records of total: %s of %s" % (str(sickcounter),str(counter))

f_output.close()
f_tweet.close()
f_pred.close()

