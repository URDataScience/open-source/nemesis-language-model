#!/bin/sh
downloaderdir="/p/twitter3/lasvegas/downloader/"
labeldir="/p/twitter3/lasvegas/labeler"
prefix="/tmp/labeler"
today=$(date +"%Y-%m-%d::%H:%M:%S")".txt"
outputfile=$prefix"-"$today
echo $outputfile
/p/twitter3/lasvegas/labeler/scripts/labelfilesv2.sh $downloaderdir $labeldir  >& $outputfile
returncode=$(echo $?)

if [ $returncode -gt 0 ]; then
	echo "Time to phone home" | mailx -a $outputfile -s "Label Process Failed" eric@answerinformatics.com
else
	echo "All is well" | mailx -a $outputfile -s "Label Process Successful" eric@answerinformatics.com
fi







